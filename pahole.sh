. ./funcs.sh || exit 1

X86_64=$cross_data/bin/x86_64-linux/bin/x86_64-linux-gcc
X86_32=$cross_data/bin/i686-linux/bin/i686-linux-gcc
ARM64=$cross_data/bin/aarch64-linux-gnu/bin/aarch64-linux-gnu-gcc
ARM32=$cross_data/bin/arm-linux-gnueabi/bin/arm-linux-gnueabi-gcc
inc="-I $top/usr/include -I /usr/include"

cd $top
rm -rf $top/usr
rm -f *.pa pahole*.o

cd $build_data/media-git

echo Install kernel headers...
make INSTALL_HDR_PATH=$top/usr headers_install >/dev/null

cd $top

($X86_64 -o pahole-i64.o $inc -c -g pahole.c &&
$X86_32 -o pahole-i32.o $inc -c -g pahole.c &&
$ARM64 -o pahole-a64.o $inc -c -g pahole.c &&
$ARM32 -o pahole-a32.o $inc -c -g pahole.c) || exit 1

ret=0
cd $top
pahole pahole-i64.o >i64.pa
pahole pahole-i32.o >i32.pa

pahole pahole-a64.o >a64.pa
pahole pahole-a32.o >a32.pa

if ! cmp i32.pa i64.pa ; then
	echo intel 32 vs 64 bit differences
	ret=1
fi
if ! cmp a32.pa a64.pa ; then
	echo arm 32 vs 64 bit differences
	ret=1
fi
if egrep 'hole|padding:' i64.pa ; then
	echo holes or padding in intel 64 bit structs
	ret=1
fi
if egrep 'hole|padding:' i32.pa ; then
	echo holes or padding in intel 32 bit structs
	ret=1
fi
if egrep 'hole|padding:' a64.pa ; then
	echo holes or padding in arm 64 bit structs
	ret=1
fi
if egrep 'hole|padding:' a32.pa ; then
	echo holes or padding in arm 32 bit structs
	ret=1
fi
if [ $ret == 0 ]; then
	rm -f *.pa pahole*.o
	rm -rf $top/usr
fi
exit $ret
