#!/bin/bash

. $top/env-upload.sh

cd $top

mkdir -p $logdir/history
cp $logdir/summary $logdir/history/$logdate
mv $logdir/summary $logdir/$logday.log

size=`ls -s $logdir/$logday.log |cut -f1 -d' '`

if [ $daily_test -eq 1 ]; then
cat >$logdir/mail << EOF
From: "$name" <$email>
To: $email
EOF
else
to=$ml

if [ -f $logdir/history/hashes ]; then
	if cmp -s $logdir/hashes $logdir/history/hashes; then
		to=$email
	fi
fi
cat >$logdir/mail << EOF
From: "$name" <$email>
To: $to
EOF
fi
if [ $daily -eq 1 -a ! -z "$msmtp_config" ]; then
	cp $logdir/hashes $logdir/history/hashes
fi

cat >>$logdir/mail << EOF
Subject: cron job: media_tree daily build: $result

This message is generated daily by a cron job that builds media_tree for
the architectures in the list below.

Results of the daily build of media_tree:

EOF

if [ $size -le 40 ]; then
	cat $logdir/$logday.log >>$logdir/mail
else
	cat $logdir/header >>$logdir/mail
	egrep '^[^ ]*: (WARNINGS)|(ERRORS|ABI WARNING|ABI ERROR|ABI OK|OK)' $logdir/$logday.log | sed 's/:$//' >>$logdir/mail
fi

if [ $size -lt 500 ]; then
	cat >>$logdir/mail << EOF

Detailed results are available here:

$website/logs/$logday.log

Detailed regression test results are available here:

$website/logs/$logday-test-media-64.log
$website/logs/$logday-test-media-64-dmesg.log
$website/logs/$logday-test-media-32.log
$website/logs/$logday-test-media-32-dmesg.log

Full logs are available here:

$website/logs/$logday.tar.bz2
EOF
else
	cat >>$logdir/mail << EOF

Logs weren't copied as they are too large ($size kB)
EOF
fi

cat >>$logdir/mail << EOF

The Media Infrastructure API from this daily build is here:

$website/spec/index.html
EOF

tar cjf $top/$logday.tar.bz2 --exclude '*.tar.bz2' $logname
mv $top/$logday.tar.bz2 $logdir
tar cjf $logdir/scripts.tar.bz2 --exclude=env.sh README *.sh *.pl env.tmpl abi configs virtme-scripts

cat $logdir/mail

if [ $daily -eq 1 ]; then
	if [ $daily_test -eq 0 -a ! -z "$lftp_args" ]; then
		cd $build_data/media-git
		rm -rf Documentation/output/media/userspace-api/v4l/_images
		mkdir -p Documentation/output/media/userspace-api/v4l/_images
		mv Documentation/output/media/_images/math Documentation/output/media/userspace-api/v4l/_images
		lftp -e "mirror -eR Documentation/output/media/ WWW/spec; quit;" $lftp_args
		cd $top
	fi
	if [ $daily_test -eq 0 -a ! -z "$lftp_args" -a $size -lt 500 ]; then
		lftp -e "mput -O WWW/logs $logdir/$logday.log $logdir/$logday.tar.bz2 $logdir/scripts.tar.bz2 $logdir/$logday-test-media-64.log $logdir/$logday-test-media-32.log $logdir/$logday-test-media-64-dmesg.log $logdir/$logday-test-media-32-dmesg.log; quit;" $lftp_args
		lftp -e "mput -O WWW/edid-decode $build_data/edid-decode/emscripten/edid-decode.html $build_data/edid-decode/emscripten/edid-decode.ico $build_data/edid-decode/emscripten/edid-decode.js $build_data/edid-decode/emscripten/edid-decode.wasm; quit;" $lftp_args
	fi
	if [ ! -z "$msmtp_config" ]; then
		msmtp -a $msmtp_config -t <$logdir/mail
	fi
fi
