
source ci/docker-repos.sh

GIT_REPO=${TEST_GIT_REPOSITORY:-"https://git.linuxtv.org/media_tree.git"}
GIT_BRANCH=${TEST_GIT_BRANCH:-"master"}

# Check if the remote is already present
for repo in ${GIT_REPOS}; do
	REMOTE="$(echo $repo | cut -d, -f1)"
	BRANCH="$(echo $repo | cut -d, -f2)"
	NAME="$(echo $repo | cut -d, -f3)"
	
	if [ "${REMOTE}" == "${GIT_REPO}" ] || [ "${NAME}" == "${GIT_REPO}" ]; then
		TEST_REMOTE="${NAME}"
	fi
done


