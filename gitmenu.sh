#!/bin/bash

. ./funcs.sh || exit 1

if [ -z "$1" ]; then
	echo "Usage: gitmenu.sh arch|all"
	echo
	echo "arch|all: update the .config by calling 'make menuconfig' for the"
	echo "          specified architecture or all."
	exit 0
fi

arch=$1
config=$1
if [ "$arch" = "sparse" -o "$arch" = "virtme" -o "$arch" = "no-pm" -o "$arch" = "no-pm-sleep" -o "$arch" = "no-of" -o "$arch" = "no-acpi" -o "$arch" == "no-debug-fs" ]; then
	arch=x86_64
fi

if [ "$arch" = "all" ]; then
	archs=`(cd $build_data/trees; echo *)`
	for arch in $archs sparse virtme no-pm no-pm-sleep no-of no-acpi no-debug-fs; do
		if [ -f $top/configs/$arch.config ]; then
			./gitmenu.sh $arch
		fi
	done
	exit 0
fi

if [ ! -f $top/configs/$config.config ]; then
	echo cannot find $top/configs/$config.config
	exit 1
fi

setup_arch $arch
opts="ARCH=$a CROSS_COMPILE=$cross-"

cd $build_data/media-git
make -j$CPUS mrproper
cd ..
cp $top/configs/$config.config $build_data/trees/$arch/media-git/.config
make -j$CPUS -C $build_data/trees/$arch/media-git $opts menuconfig &&
make -j$CPUS -C $build_data/trees/$arch/media-git $opts prepare &&
cp $build_data/trees/$arch/media-git/.config $top/configs/$config.config
